<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop.php or
 * loop-template.php, where 'template' is the loop context
 * requested by a template. For example, loop-index.php would
 * be used if it exists and we ask for the loop with:
 * <code>get_template_part( 'loop', 'index' );</code>
 *
 * @package WordPress
 * @subpackage RotorWash
 * @since RotorWash 1.0
 */

/* If there are no posts to display, such as an empty archive page */
if (!have_posts()):

?>
    <article class="post">
        <h2>No Posts Here</h2>
        <p>
            Sorry, but there are no posts here. 
            <a href="<?php echo home_url('/'); ?>">Back to the home page.</a>
        </p>
    </article>
<?php

else:
    while (have_posts()):
        the_post();

        $link_title = sprintf(
            esc_attr('Permalink to %s'), 
            the_title_attribute('echo=0')
        );

?>
    <article class="post preview">

        <h2>
            <a href="<?php the_permalink(); ?>" 
               title="<?php echo $link_title; ?>" 
               rel="bookmark"><?php the_title(); ?></a>
        </h2>

        <?php the_excerpt(); ?>
        <? if (get_post_type()==='post'): ?> 
        <div class="post-meta">
            <small><?php rw_posted_on(); ?></small>
        </div>
        <? endif; ?>

    </article>
<?php

    endwhile;
endif;

// Displays pagination nav when applicable
if ($wp_query->max_num_pages>1) {
    rw_pagination();
}
