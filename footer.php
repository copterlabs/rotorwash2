<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @subpackage RotorWash
 * @since RotorWash 1.0
 */

$title = esc_attr(get_bloginfo('name')) . ' &mdash; '
       . esc_attr(get_bloginfo('description'));

$site_credit = rw_footer_credit_link("Site by Copter Labs");

?>

</div>

<footer id="site-credits" class="container">
    <div class="row">
        <p class="col-sm-12 clearfix text-muted credit">
            <small class="pull-left">
                All content copyright &copy; 
                <a href="<?php echo home_url('/'); ?>" 
                   title="<?php echo $title; ?>" 
                   rel="home"><?php bloginfo('name', 'display'); ?></a>
            </small>
            <small class="pull-right">
                <?php echo $site_credit; ?> 
            </smalL>
        </p>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
