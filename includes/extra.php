<?php
/**
 * Utility functions to handle common tasks and special functionality.
 */

if (!function_exists('rw_get_wrapper_class')):
/**
 * Determines the page or post slug
 * @return string The page or post slug
 * @since  2.0.0
 */
function rw_get_wrapper_class(  ) {
    global $wp_query;
    $slug = 'default';

    $post_obj = $wp_query->get_queried_object();
    if (is_object($post_obj)) {
        // Gets the post slug
        if (property_exists($post_obj, 'post_name')) {
            $slug = $post_obj->post_name;
        }

        // Posts should end up under the "blog" umbrella class
        if (
            (property_exists($post_obj, 'post_type')
            && $post_obj->post_type==='post')
            || is_category() 
            || is_author()
        ) {
            $slug = 'blog';
        }
    }

    return $slug;
}
endif;

if (!function_exists('rw_posted_on')):
/**
 * Prints HTML with meta information for the current post—date/time and author.
 * @return  void
 * @since   1.0.0
 */
function rw_posted_on( ) {
    printf(
        '<span class="%1$s">Posted on</span> %2$s <span class="meta-sep">by</span> %3$s',
        'meta-prep meta-prep-author',
        sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
            get_permalink(),
            esc_attr( get_the_time() ),
            get_the_date()
        ),
        sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
            get_author_posts_url( get_the_author_meta( 'ID' ) ),
            sprintf( esc_attr__( 'View all posts by %s', 'rotorwash' ), get_the_author() ),
            get_the_author()
        )
    );
}
endif;

if (!function_exists('rw_posted_in')):
/**
 * Prints HTML with metadata for the current post (category, tags & permalink)
 * @param   bool $show_tags Whether tags should be displayed (default: TRUE)
 * @return  void
 * @since   1.0.0
 */
function rw_posted_in( $show_tags=TRUE ) {
    $tag_list = get_the_tag_list( '', ', ' );

    if ($tag_list && $show_tags) {
        $posted_in = __( 'Category: %1$s Tags: %2$s', 'rotorwash' );
    } elseif(is_object_in_taxonomy(get_post_type(), 'category')) {
        $posted_in = __( 'Category: %1$s', 'rotorwash' );
    } else {
        $posted_in = NULL;
    }

    printf($posted_in, get_the_category_list( ', ' ), $tag_list);
}
endif;

if (!function_exists('rw_footer_credit_link')):
/**
 * Creates a link with Google Analytics campaign info
 * @param  string $text The text to use for the link
 * @return string The URI with Google Analytics campaign info
 * @since  2.0.0
 */
function rw_footer_credit_link( $text=NULL, $title=NULL ) {
    // Adds default text if none is supplied
    if (empty($text)) {
        $text = "Web design by Copter Labs";
    }

    // Adds a default title if none is supplied
    if (empty($title)) {
        $title = "Marketing, design, and devleopment in Portland, OR";
    }

    // Generates and returns a Google Analytics-friendly footer link
    return rw_create_ga_link(
        'http://www.copterlabs.com/',
        $text,
        $title,
        sanitize_title(get_bloginfo('name')),
        'footerlink',
        'clientreferral'
    );
}
endif;

/**
 * Creates a Google Analytics campaign link
 *
 * NOTE: The protocol (i.e. `http://`) is required for proper operation.
 * 
 * @param  string  $uri      The URI for which the link should be generated
 * @param  string  $text     The text for the link
 * @param  string  $title    The title for the link
 * @param  string  $source   Google Analytics UTM source
 * @param  string  $medium   Google Analytics UTM medium
 * @param  string  $campaign Google Analytics UTM campaign
 * @param  boolean $uri_only Whether or not to generate markup for the link
 * @return string            The link markup or URI
 * @since  2.0.0
 */
function rw_create_ga_link(
    $uri,
    $text     = NULL,
    $title    = NULL,
    $source   = 'site-link',
    $medium   = 'generated-link',
    $campaign = 'rotorwash2',
    $uri_only = FALSE
) {
    // Breaks apart the URI and makes sure not to break hashes or queries
    extract(parse_url($uri));
    $scheme   = isset($scheme)   ? $scheme . '://'  : 'http://';
    $host     = isset($host)     ? $host            : 'google.com';
    $path     = isset($path)     ? $path            : '/';
    $query    = isset($query)    ? '&' . $query     : NULL;
    $fragment = isset($fragment) ? '#' . $fragment  : NULL;

    // Recombines the URI pieces (without the query or fragment/hash)
    $uri = $scheme . $host . $path;

    // Generates the new URI
    $link_uri = sprintf(
        '%s?utm_source=%s&utm_medium=%s&utm_campaign=%s%s%s', 
        $uri, $source, $medium, $campaign, $query, $fragment
    );

    // If set, returns only the URI; otherwise returns markup
    if ($uri_only) {
        return $link_uri;
    } else {
        // Checks for link text and a title
        $text  = isset($text)  ? $text : $uri;
        $title = isset($title) ? ' title="' . $title . '"' : NULL;

        return sprintf('<a href="%s"%s>%s</a>', $link_uri, $title, $text);
    }
}


/**
 * Bootstrap Numbered Pagination
 *
 * @param  int    $range  Amount of pages to the left and right of active page
 * @param  int    $pages  Total pages, for use with custom loops
 * @param  string $prev   Markup to use for the previous page button
 * @param  string $next   Markup to use for the next page button
 * @param  bool   $return Flag to set if the markup should echo or be returned
 * @return mixed          The markup if $return is TRUE, else void
 */
function rw_pagination( $text_align='center', $range=4, $pages=NULL, $prev=NULL, $next=NULL, $return=TRUE ) { 
    if (empty($prev)) {
        $prev = '&lsaquo;';
    }

    if (empty($next)) {
        $next = '&rsaquo;';
    }

    // Gets the current page
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }

    // Loads the number of pages from the loop if not set
    if (empty($pages)) {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }   

    // Creates the pagination
    $pagination = NULL;
    if ($pages>1) {
        $page_array = array();

        $first_page = $paged-$range>0 ? $paged-$range : 1;
        $last_page = $first_page+$range*2<$pages ? $first_page+$range*2 : $pages;

        // Adds the "previous" button
        if ($paged>1) {
            $page_array[] = '<li class="first">'
                          . '<a href="' . get_pagenum_link(1)
                          . '" title="First Page">&laquo;</a></li>';
            $page_array[] = '<li>'
                          . '<a href="' . get_pagenum_link($paged-1) 
                          . '">' . $prev . '</a></li>';
        }

        for ($i=$first_page; $i<=$last_page; ++$i) {
            if ($pages>1 && ($i<$paged+$range+1 || $i>$paged-$range-1)) {
                $active_class = $paged===$i ? ' class="active"' : NULL;
                $page_array[] = '<li' . $active_class . '>'
                              . '<a href="'
                              . get_pagenum_link($i) . '">' . $i . '</a>'
                              . '</li>';
            }
        }

        // Adds the "next" button
        if ($paged<$pages) {
            $page_array[] = '<li>'
                          . '<a href="' . get_pagenum_link($paged+1) 
                          . '" title="Next Page">' . $next . '</a></li>';
            $page_array[] = '<li class="last">'
                          . '<a href="' . get_pagenum_link($pages)
                          . '" title="Last Page">&raquo;</a></li>';
        }

        $pagination = '<div class="text-'.$text_align.'"><ul class="pagination">'
                    . implode("\n    ", $page_array)
                    . '</ul></div>';
    }

    if ($return===TRUE) {
        echo $pagination;
    } else {
        return $pagination;
    }
}



/**
 * Creates a short link using Bitly's API
 *
 * NOTE: In order for this function to actually shorten the URL, a Bitly 
 * access token needs to be obtained and added via the "Advanced Settings" 
 * page under "Site Settings" on the WordPress Dashboard.
 *
 * If no token is present, the unshortened URL is returned to prevent errors.
 * 
 * @param  string $url The long URL to be shortened
 * @return string      The shortened URL
 * @since  2.2.0
 */
function rw_create_short_link( $url ) {
    $bitly_api_uri = 'https://api-ssl.bitly.com/v3/';

    // Loads the access token from the options page (Advanced Settings)
    $access_token = get_field('bitly_access_token', 'option');

    // Make sure the access token is set to avoid errors
    if (!empty($access_token)) {
        // Builds the API request URL
        $api_request = sprintf(
            '%sshorten?access_token=%s&longUrl=%s', 
            $bitly_api_uri, 
            $access_token, 
            urlencode($url)
        );

        // To avoid unnecessary API requests, we're using transients
        $transient_name = sha1('bitly_link_' . $url);
        $shortened = get_transient($transient_name);
        if (empty($shortened)) {
            $response = json_decode(rw_curl_get_result($api_request));

            // If the URL came back, store it
            if (is_object($response) && $response->status_code===200) {
                $shortened = 'http://bit.ly/' . $response->data->hash;
                set_transient($transient_name, $shortened, 7*DAY_IN_SECONDS);
                return $shortened;
            }
        } else {
            $url = $shortened;
        }
    }

    // If the request failed, return the full URL to avoid errors
    return $url;
}

/**
 * Gets the result of a cURL request to an endpoint
 * @param  string $endpoint The API enpoint to hit
 * @return mixed            The response
 */
function rw_curl_get_result( $endpoint ) {
    // Bails if cURL isn't installed
    if (!function_exists('curl_version')) {
        return FALSE;
    }

    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

/**
 * Load wp_bootstrap_navwalker for bootstrap navigation goodness
 * 
 */
require_once TEMPLATEPATH . '/assets/lib/wp-bootstrap-navwalker/wp_bootstrap_navwalker.php';


