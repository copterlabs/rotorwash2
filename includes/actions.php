<?php
/**
 * RotorWash actions
 *
 * @package     WordPress
 * @subpackage  RotorWash
 * @since       1.0
 */

/**
 * Adds Facebook Open Graph tags to the website using the wp_head action.
 *
 * Depending on what type of post/page we're displaying, different information will
 * be used. This makes for those pretty-looking shared links in the Facebook timeline.
 * 
 * @see     http://codex.wordpress.org/Plugin_API/Action_Reference/wp_head
 * @since   1.0.2
 * @return  void
 */
function rw_add_fb_og_tags(  ) {
    // Bails if Yoast SEO or Facebook is installed to avoid duplicate OG tags
    include_once ABSPATH . 'wp-admin/includes/plugin.php';
    if (
        is_plugin_active('wordpress-seo/wp-seo.php') 
        || is_plugin_active('facebook/facebook.php')
    ) {
        return FALSE;
    }

    // Loads settings
    $opts = get_option('rw_theme_settings');

    // Avoids a warning in the Facebook URL linter
    $locale     = strtolower(get_locale());
    $site_name  = get_bloginfo('name');

    // Adds Facebook admins if present
    if (!empty($opts['fb_admins'])) {
        $fb_admins = '<meta property="fb:admins"      '
                   . 'content="'.$opts['fb_admins'].'" />' 
                   . PHP_EOL;
    } else {
        $fb_admins = NULL;
    }

    // Adds the app ID if one is supplied
    if (!empty($opts['fb_appid'])) {
        $fb_appid = '<meta property="fb:app_id"      '
                  . 'content="'.$opts['fb_appid'].'" />' 
                  . PHP_EOL;
    } else {
        $fb_appid = NULL;
    }

    // Checks for a default image set in the custom theme settings
    $def_img   = !empty($opts['default_image']) ? $opts['default_image'] : '';

    if (is_single()) {
        global $post; // Brings the post into the function scope
        if (get_the_post_thumbnail($post->ID, 'thumbnail')) {
            $thumbnail_id = get_post_thumbnail_id($post->ID, 'thumbnail');
            $thumbnail_object = get_post($thumbnail_id);
            $image = $thumbnail_object->guid;
        } else {
            $image = $def_img;
        }

        // Generates an excerpt if one doesn't exist
        if (!empty($post->post_excerpt)) {
            $excerpt = $post->post_excerpt;
        } else {
            $excerpt = apply_filters('get_the_excerpt', $post->post_content);
        }

        // Gets entry-specific info for display
        $title       = $post->post_title;
        $url         = get_permalink($post->ID);
        $type        = "article";
        $description = trim(strip_tags($excerpt));
    } else {
        // Non-blog posts/pages (home page, loops, etc.) display site info only
        $title       = $site_name;
        $url         = site_url();
        $image       = $def_img;
        $type        = "website";
        $description = get_bloginfo('description');
    }

    // Output the OG tags directly
?>

<!-- Facebook Open Graph tags -->
<meta property="og:url"         content="<?php echo $url; ?>" />
<meta property="og:type"        content="<?php echo $type; ?>" />
<meta property="og:title"       content="<?php echo $title; ?>" />
<meta property="og:locale"      content="<?php echo $locale; ?>" />
<meta property="og:image"       content="<?php echo $image; ?>" />
<meta property="og:description" content="<?php echo $description ?>" />
<meta property="og:site_name"   content="<?php echo $site_name; ?>" />
<?php echo $fb_admins,$fb_appid; ?>

<?php
}
add_action('wp_head', 'rw_add_fb_og_tags');

/**
 * Enqueues scripts and stylesheets for the theme
 *
 * By default, RotorWash2 enqueues a local copy of:
 *  * jQuery
 *  * ShareThis (will be removed in v3.0.0)
 *  * Bootstrap.js
 *  * A main JS (`assets/js/main.min.js`) file that loads:
 *  ** `assets/js/init.js` -- init script for the theme (starts empty)
 *  ** `assets/js/social-init.js` -- The social initialization scripts 
 *      (loads FB, Twitter, G+, and Pinterest)
 *  * A default stylesheet (`assets/css/main.css`)
 *
 * @return  void
 * @since   1.0.0
 */
function rw_enqueue_assets(  ) {
    // Set the location of the assets folder
    $assets_dir = get_template_directory_uri() . '/assets';
    $bootstrap_version = '3.0.0';

    // Registers styles
    wp_enqueue_style(
        'rotorwash-main-styles',
        $assets_dir . '/css/main.css',
        FALSE,
        filemtime(get_template_directory() . '/assets/css/main.css')
    );

    // This is only necessary if an IE-specific stylesheet is required
    if (is_readable(ASSETS_PATH . '/css/ie.css')) {
        global $wp_styles;
        wp_enqueue_style(
            'theme-ie-styles',
            ASSETS_DIR . '/css/ie.css',
            array(),
            '1.0.0b' . filemtime(ASSETS_PATH . '/css/ie.css')
        );

        // Adds a conditional tag
        $wp_styles->add_data('theme-ie-styles', 'conditional', 'lte IE 8');
    }

    // Include a local copy of jQuery
    wp_dequeue_script('jquery');
    wp_deregister_script('jquery');

    wp_register_script(
        'jquery', 
        $assets_dir . '/lib/jquery/jquery.js', 
        NULL, 
        '2.0.3', 
        FALSE
    );

    // Main JS
    wp_enqueue_script(
        'rotorwash-main-js',
        $assets_dir . '/js/main.min.js',
        array('jquery'),
        '1.0.0',
        TRUE
    );

    // Bootstrap scripts for built-in JS components
    wp_enqueue_script(
        'bootstrap-scripts',
        $assets_dir . '/lib/bootstrap/dist/js/bootstrap.min.js',
        'jquery',
        $bootstrap_version,
        TRUE
    );
}
add_action('wp_enqueue_scripts', 'rw_enqueue_assets');

/**
 * Adds the HTML5 shim and Respond.js for IE support of HTML5 and media queries
 * @return void
 * @since  2.3.1
 */
function rw_add_ie_scripts(  ) {
    // Set the location of the assets folder
    $assets_dir = get_template_directory_uri() . '/assets';
?>
<!--[if lt IE 9]>
    <script src="<?php echo $assets_dir; ?>/lib/html5shiv/html5shiv.js"></script>
    <script src="<?php echo $assets_dir; ?>/lib/respond/respond.min.js"></script>
<![endif]-->
<?php
}
add_action('wp_head', 'rw_add_ie_scripts', 100);

/**
 * Outputs Google Analytics tracking code if an ID is supplied
 *
 * Uses the new Google Analytics. This means old sites with legacy Analytics 
 * **must** be upgraded in the Analytics control panel or tracking will not
 * work as expected.
 * 
 * @return void
 * @since 1.1.0
 */
function rw_add_google_analytics(  ) {
    $opts = get_option('rw_theme_settings');
    if(get_field('google_analytics_id', 'option')):
        // Determines the domain name
        $domain = preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']);
?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php the_field('google_analytics_id', 'option'); ?>', '<?php echo $domain; ?>');
  ga('send', 'pageview');

</script>
<?php
    endif;
}
add_action('wp_footer', 'rw_add_google_analytics');

/**
 * Adds Google fonts asynchronously
 *
 * **NOTE:** This function needs work. It should either be removed altogether 
 * or improved to allow configuration from within RW2.
 * 
 * @return void
 * @since  2.0.0
 */
function rw_add_google_fonts(  ) {
?>
<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Open+Sans+Condensed:700:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); </script>
<?php
}
add_action('wp_head', 'rw_add_google_fonts');
