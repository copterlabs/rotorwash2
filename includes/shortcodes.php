<?php

/*
 * BOOTSTRAP COMPONENTS
 *****************************************************************************/

function shortcode_bootstrap_lead( $attr, $content ) {
    $clean = strip_tags($content, '<strong><em><a><span>');
    return '<p class="lead">' . $clean . '</p>';
}
add_shortcode('lead', 'shortcode_bootstrap_lead');

function shortcode_bootstrap_alert( $attr, $content ) {
    $defaults = array(
        'type' => 'info',
    );
    extract(shortcode_atts($defaults, $attr));

    $allowed_types = array(
        'info', 'success', 'warning', 'danger',
    );

    if (!in_array($type, $allowed_types)) {
        $type = 'info';
    }

    $alert_class = 'alert alert-' . $type;

    $clean = rw_remove_crappy_markup($content);
    return '<div class="' . $alert_class . '">' . $clean . '</div>';
}
add_shortcode('alert', 'shortcode_bootstrap_alert');


/*
 * UTILITY FUNCTIONS
 *****************************************************************************/

/**
 * Removes mismatched </p> and <p> tags from the beginning and end of a snippet.
 * @param  string $string The string to clean
 * @return string         The cleaned string
 */
function rw_remove_crappy_markup( $string )
{
    $patterns = array(
        '#^\s*</p>#',
        '#<p>\s*$#'
    );
 
    return preg_replace($patterns, '', $string);
}
