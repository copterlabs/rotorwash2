<?php

/**
 * Social functions to do things like embed Tweet buttons
 */

/**
 * Returns a Twitter tweet button
 *
 * The configuration options are: 
 * `via`:       Twitter username for the author or publisher (default: `NULL`)
 * `recommend`: Twitter username to recommend to the user (default: `NULL`)
 * `hashtag`:   Hashtag to append to the tweet (default: `NULL`)
 * `text`:      The tweet text (default: `NULL`)
 * 
 * @param   string  $url    The URL to share
 * @param   array   $config Optional extras for the button
 * @return  string          The markup for the tweet button
 * @since   2.1.0
 */
function rw2_social_twitter_btn( $url, $config=array() ) {
    // Sets up defaults and allows user-defined values to override
    $default_config = array(
        'via' => NULL,
        'recommend' => NULL,
        'hashtag' => NULL,
        'text' => NULL,
    );
    $options = array_merge($default_config, $config);

    // Checks for an absolute URL
    if (strpos($url, 'http')!==0) {
        $url = home_url($url);
    }

    // Sets up a variable to contain the optional data attributes
    $data_attrs = NULL;

    if (!empty($options['via'])) {
        $data_attrs .= ' data-via="' . $options['via'] . '"';
    }

    if (!empty($options['recommend'])) {
        $data_attrs .= ' data-related="' . $options['recommend'] . '"';
    }

    if (!empty($options['hashtag'])) {
        $data_attrs .= ' data-hashtags="' . $options['hashtag'] . '"';
    }

    if (!empty($options['text'])) {
        $data_attrs .= ' data-text="' . $options['text'] . '"';
    }

    return '<a href="https://twitter.com/share" class="twitter-share-button"'
           . ' data-url="' . $url . '" ' . $data_attrs . '>Tweet</a>';
}

/**
 * Returns a Facebook like button
 *
 * The configuration options are:
 * `layout`:     How to display the button (default: `button_count`)
 * `action`:     The verb to use on the button (default: `like`)
 * `show-faces`: Whether or not faces should be shown (default: `false`)
 * `share`:      Whether or not to display the share button (default: `false`)
 * 
 * @param   string  $url    The URL to share
 * @param   array   $config Optional extras for the button
 * @return  string          The markup for the like button
 * @since   2.1.0
 */
function rw2_social_facebook_btn( $url, $config=array() ) {
    // Sets up defaults and allows user-defined values to override
    $default_config = array(
        'layout'        => 'button_count',
        'action'        => 'like',
        'show-faces'    => 'false',
        'share'         => 'false',
    );
    $options = array_merge($default_config, $config);

    // Checks for an absolute URL
    if (strpos($url, 'http')!==0) {
        $url = home_url($url);
    }

    // Sets up a variable to contain the optional data attributes
    $data_attrs = NULL;

    if (!empty($options['layout'])) {
        $data_attrs .= ' data-layout="' . $options['layout'] . '"';
    }

    if (!empty($options['action'])) {
        $data_attrs .= ' data-action="' . $options['action'] . '"';
    }

    if (!empty($options['show-faces'])) {
        $data_attrs .= ' data-show-faces="' . $options['show-faces'] . '"';
    }

    if (!empty($options['share'])) {
        $data_attrs .= ' data-share="' . $options['share'] . '"';
    }

    return '<div class="fb-like" data-href="' . $url . '" ' 
           . $data_attrs . '></div>';
}

/**
 * Returns a Google+ +1 button
 * 
 * The configuration options are:
 * `size`:         What size of button to show (default: `medium`)
 * `annotation`:   Whether or not to show annotations (default: `NULL`)
 * `width`:        What width to display the button (default: `NULL`)
 * 
 * @param   string  $url    The URL to share
 * @param   array   $config Optional extras for the button
 * @return  string          The markup for the +1 button
 * @since   2.1.0
 */
function rw2_social_gplus_btn( $url, $config=array() ) {
    // Sets up defaults and allows user-defined values to override
    $default_config = array(
        'size'          => 'medium',
        'annotation'    => NULL,
        'width'         => NULL,
    );
    $options = array_merge($default_config, $config);

    // Checks for an absolute URL
    if (strpos($url, 'http')!==0) {
        $url = home_url($url);
    }

    // Sets up a variable to contain the optional data attributes
    $data_attrs = NULL;

    if (!empty($options['size'])) {
        $data_attrs .= ' data-size="' . $options['size'] . '"';
    }

    if (!empty($options['annotation'])) {
        $data_attrs .= ' data-annotation="' . $options['annotation'] . '"';
    }

    if (!empty($options['width'])) {
        $data_attrs .= ' data-width="' . $options['width'] . '"';
    }

    return '<div class="g-plusone" data-href="' . $url . '" ' 
           . $data_attrs . '></div>';
}

/**
 * Returns a Pinterest Pin It button
 *
 * The configuration options are:
 * `description`: The pin's description (default: `get_bloginfo('name')`)
 * `pin-do`:      Action for the button to perform (default: `buttonPin`)
 * `pin-config`:  Where to place the pin count (default: `beside`)
 * 
 * @param  string $url      The URL to share
 * @param  string $img      The image to pin
 * @param  array  $config   Optional extras for the button
 * @return string           The markup for the Pin It button
 * @since  2.1.0
 */
function rw2_social_pinterest_btn( $url, $img, $config=array() ) {
    // Sets up defaults and allows user-defined values to override
    $default_config = array(
        'description'   => get_bloginfo('name'),
        'pin-do'        => 'buttonPin',
        'pin-config'    => 'beside',
    );
    $options = array_merge($default_config, $config);

    // Checks for an absolute URL
    if (strpos($url, 'http')!==0) {
        $url = home_url($url);
    }

    // Encodes the URL
    $enc_url = urlencode($url);
    $enc_img = urlencode($img);

    // Sets up a variable to contain the optional data attributes
    $data_attrs = NULL;

    if (!empty($options['description'])) {
        $enc_desc = urlencode($options['description']);
    }

    if (!empty($options['pin-do'])) {
        $data_attrs .= ' data-pin-do="' . $options['pin-do'] . '"';
    }

    if (!empty($options['pin-config'])) {
        $data_attrs .= ' data-pin-config="' . $options['pin-config'] . '"';
    }

    return '<a href="//www.pinterest.com/pin/create/button/?url=' . $enc_url 
           . '&media=' . $enc_img . '&description=' . $enc_desc . '" '
           . $data_attrs . ' class="rw-pinterest">'
           . '<img src="//assets.pinterest.com/images/pidgets/pin_it_button.png">'
           . '</a>';
}


function twitter_intents_tweet( $text, $link=NULL, $config=array() ) {
    $url_base = 'https://twitter.com/intent/tweet';

    $defaults = array(
        'via'         => NULL,
        'in_reply_to' => NULL,
        'hashtags'    => NULL,
        'related'     => NULL,
    );

    $config = array_replace($defaults, $config);

    $url = $url_base . '?text=' . urlencode($text);

    if ($link!==NULL) {
        $url .= '&url=' . urlencode($link);
    }

    foreach ($config as $key => $val) {
        if (!empty($val)) {
            $url .= '&' . $key . '=' . urlencode($val);
        }
    }

    return $url;
}
