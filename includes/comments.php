<?php

/**
 * A custom walker to output comments using Bootstrap 3 markup
 * @param  object $comment The comment object
 * @param  array  $args    Optional arguments
 * @param  int    $depth   How many levels of nested comments are allowed
 * @return void
 */
function rw2_comment_walker( $comment, $args, $depth ) {
    global $post_id;

    // Stores various comment information
    $comment_ID_attr = 'comment-' . get_comment_ID();
    $comment_link = get_permalink() . '#' . $comment_ID_attr;
    $comment_date = sprintf('%s at %s', get_comment_date(), get_comment_time());

    // Checks if the commenter is the post author
    $is_author = FALSE;
    $author_badge = NULL;
    if ($post=get_post($post_id)) {
        if ($comment->user_id===$post->post_author) {
            $is_author = TRUE;
            $author_badge =' <small><span class="label label-info">Author</span></small>';
        }
    }

    // For reply links
    if ($args['style']==='div') {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }

    // Generates the comment reply link
    $comment_reply_custom_config = array(
        'reply_text' => 'Reply to this comment',
        'add_below' => $add_below,
        'depth' => $depth,
        'max_depth' => $args['max_depth'],
        'before' => '| ',
    );
    $comment_reply_config = array_merge($args, $comment_reply_custom_config);
    $comment_reply_link = get_comment_reply_link($comment_reply_config);

    // Generates the avatar
    if ($args['avatar_size']!==0) {
        $avatar = get_avatar($comment, $args['avatar_size']);
    } else {
        $avatar = NULL;
    }

    // Adds a note if the comment is awaiting moderation
    if ($comment->comment_approved==='0') {
        $moderation_notice = '<span class="label label-default">'
                           . 'awaiting moderation'
                           . '</span>';
    } else {
        $moderation_notice = NULL;
    }
?>
    <<?php echo $tag; ?> id="<?php echo $comment_ID_attr; ?>" 
         class="media">
        <div class="pull-left">
            <?php echo $avatar; ?> 
        </div>
        <div class="media-body comment-author">
            <h4 class="media-heading">
                <?php echo get_comment_author_link(), $author_badge; ?> 
            </h4>
            <?php comment_text(); ?> 
            <p>
                <small>
                    <a href="<?php echo $comment_link; ?>">
                        <?php echo $comment_date; ?> 
                    </a>
                    <?php echo $comment_reply_link; ?> 
                    <?php echo $moderation_notice; ?> 
                </small>
            </p>
        </div>

<?php
}

/**
 * Generates markup for a Bootstrap 3 form
 * @param  array  $args    Optional arguments
 * @param  int    $post_id The post ID
 * @return void
 */
function rw2_comments_form( $args=array(), $post_id=NULL ) {
    $post_id = $post_id===NULL ? get_the_ID() : $post_id;

    // Sets up variables for the function
    $commenter = wp_get_current_commenter();
    $user = wp_get_current_user();
    $user_identity = $user->exists() ? $user->display_name : '';

    // Generates a notice for logged-in users
    $logged_in = sprintf(
        'Logged in as <a href="%1$s" class="alert-link">%2$s</a>. <a href="%3$s" title="Log out of this account" class="alert-link">Log out?</a>', 
        get_edit_user_link(), 
        $user_identity, 
        wp_logout_url(apply_filters('the_permalink', get_permalink($post_id)))
    );
    $logged_in_notice = apply_filters(
        'comment_form_logged_in', 
        $logged_in, 
        $commenter, 
        $user_identity
    );

?>
<form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" 
      method="post" 
      role="form" 
      id="comment-form" 
      class="comment-form"  novalidate>
<?php do_action('comment_form_top'); ?>
<?php do_action('comment_form_before_fields'); ?>
    <div class="row">
        <div class="col-sm-12">
            <h2>Leave a Comment</h2>
        </div>
<?php if (!is_user_logged_in()): ?>
        <div class="form-group col-sm-4">
            <label for="author" 
                   class="required"><?php echo __('Name'); ?></label>
            <input id="author" 
                   name="author" 
                   type="text" 
                   class="form-control" 
                   placeholder="e.g. John Doe" 
                   value="<?php echo $commenter['comment_author']; ?>"
                   aria-required="true">
        </div>
        <div class="form-group col-sm-4">
            <label for="email" 
                   class="required"><?php echo __('Email'); ?></label>
            <input id="email" 
                   name="email" 
                   type="email" 
                   class="form-control" 
                   placeholder="e.g. john@example.com" 
                   value="<?php echo $commenter['comment_author_email']; ?>"
                   aria-required="true">
        </div>
        <div class="form-group col-sm-4">
            <label for="url"><?php echo __('Website'); ?></label>
            <input id="url" 
                   name="url" 
                   type="url" 
                   class="form-control" 
                   placeholder="http://" 
                   value="<?php echo $commenter['comment_author_url']; ?>">
        </div>
<?php else: ?>
        <div class="col-sm-12">
            <div class="alert alert-info">
                <?php echo $logged_in_notice; ?> 
<?php do_action('comment_form_logged_in_after', $commenter, $user_identity); ?>
            </div>
        </div>
<?php endif; ?>
    </div>
    <div class="row">
        <div class="form-group col-sm-12">
            <label for="comment" 
                   class="required"><?php echo __('Comment'); ?></label>
            <textarea id="comment" 
                      name="comment" 
                      type="email" 
                      class="form-control" 
                      rows="3" 
                      aria-required="true"></textarea>
        </div>
    </div>
<?php do_action('comment_form_after_fields'); ?>
    <button type="submit"
            class="btn btn-primary">Post Comment</button>
<?php do_action('comment_form'); ?>
<?php comment_id_fields(get_the_ID()); ?>
</form>
<?php
}
