<?php

/**
 * Register custom styles for the admin dashboard widget
 * @return void
 * @since 1.1.0
 */
function rw_admin_init(  ) {
    // Custom styles for the Dashboard
    wp_enqueue_style(
        'rotorwash_admin_styles', 
        get_template_directory_uri() . '/assets/css/admin.css'
    );

    // Registers custom settings pages and fields
    register_custom_settings();

    // Creates the Developer role
    $role = new RW_Role;
}
add_action('admin_init', 'rw_admin_init');

/**
 * Adds the theme settings button to the dashboard
 * @return  void
 * @since   1.0.2
 */
function rw_create_menu_item(  ) {
    add_options_page(
        'Settings for ' . wp_get_theme(), 
        'RW2 Settings', 
        'manage_rotorwash', 
        'rotorwash_general', 
        'rw_settings_page'
    );

    register_setting('rw-theme-settings', 'rw_theme_settings');
}
add_action('admin_menu', 'rw_create_menu_item');

/**
 * Registers the custom settings with WordPress
 * @return  void
 * @since   1.0.2
 */
function register_custom_settings(  ) {
    // Theme Settings
    add_settings_section(
        'rw-theme-settings', 
        'Theme Settings', 
        'rw_theme_settings_text', 
        'rotorwash_general'
    );

    add_settings_field(
        'has_products', 
        'Add the Products custom post type', 
        'rw_has_products', 
        'rotorwash_general', 
        'rw-theme-settings', 
        array('label_for'=>'has_products')
    );

    add_settings_field(
        'has_services', 
        'Add the Services custom post type', 
        'rw_has_services', 
        'rotorwash_general', 
        'rw-theme-settings', 
        array('label_for'=>'has_services')
    );

    add_settings_field(
        'has_testimonials', 
        'Add the Testimonials custom post type', 
        'rw_has_testimonials', 
        'rotorwash_general', 
        'rw-theme-settings', 
        array('label_for'=>'has_testimonials')
    );

    do_action('custom_settings_hook');
}

/**
 * Adds a message for the top of the theme settings
 * @return  void
 * @since   1.0.2
 */
function rw_theme_settings_text(  ) {
?>

<p>
    If this theme needs a Products, Services, or Testimonials custom post 
    type, activate them here.
</p>
<p>
    To override the default custom fields for a custom post type below, add 
    the appropriate code to the top of the child theme's 
    <code>functions.php</code>:
</p>
<pre class="rw2-code">
// Prevents RW2 from setting up the Products custom fields
define('RW2_SKIP_PRODUCT_FIELDS', TRUE);

// Prevents RW2 from setting up the Services custom fields
define('RW2_SKIP_SERVICE_FIELDS', TRUE);

// Prevents RW2 from setting up the Testimonials custom fields
define('RW2_SKIP_TESTIMONIAL_FIELDS', TRUE);
</pre>

<?php
}

/**
 * Sets whether or not a products page is required
 * @return  void
 * @since   1.0.2
 */
function rw_has_products(  ) {
    rw_radio(__FUNCTION__);
}

/**
 * Sets whether or not a services page is required
 * @return  void
 * @since   1.0.2
 */
function rw_has_services(  ) {
    rw_radio(__FUNCTION__);
}

/**
 * Sets whether or not a testimonials page is required
 * @return  void
 * @since   1.0.2
 */
function rw_has_testimonials(  ) {
    rw_radio(__FUNCTION__);
}

/**
 * Creates a checkbox for the Theme settings page
 * @return  void
 * @since   1.0.1
 */
function rw_radio( $func ) {
    $field = str_replace('rw_', '', $func);
    $opts  = get_option('rw_theme_settings');
    $yes   = (isset($opts[$field]) && $opts[$field]==='yes') ? 'checked' : '';
    $no    = $yes==='checked'  ? '' : 'checked';
?>

<label>
    <input type="radio" 
           id="<?php echo $field; ?>_on" 
           name="rw_theme_settings[<?php echo $field; ?>]" 
           value="yes" <?php echo $yes; ?> />
    Yes
</label>
<label>
    <input type="radio" 
           id="<?php echo $field; ?>_off" 
           name="rw_theme_settings[<?php echo $field; ?>]" 
           value="no" <?php echo $no; ?> />
    No
</label>

<?php
}

/**
 * Loads the custom theme settings area
 * @return  void
 * @since   1.0.1
 */
function rw_settings_page(  ) {
    $page_title = "Settings for " . wp_get_theme();
    $custom_settings = 'rotorwash_general';
    require_once TEMPLATEPATH . '/includes/rotorwash-settings.php';
}

/**
 * Removes unnecessary dashboard home widgets
 * @return  void
 * @since   1.1.0
 */
function rw_update_dashboard_widgets(  ) {
    global $wp_meta_boxes;

    // This shouldn't be available to users without minimum clearance
    if (current_user_can('edit_posts')) {
        $widget_title = 'Helpful Links for Your New Site by Copter Labs';
        wp_add_dashboard_widget('rotorwash_dashboard', $widget_title, 'rw_add_dashboard_widget');
    }

    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
    remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'normal');
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
    remove_meta_box('dashboard_primary', 'dashboard', 'side');
    remove_meta_box('dashboard_secondary', 'dashboard', 'side');
}
add_action('wp_dashboard_setup', 'rw_update_dashboard_widgets' );

/**
 * Adds a custom widget to the WordPress Dashboard
 * @return void
 * @since  1.0.0
 */
function rw_add_dashboard_widget(  ) {
?>
<p>
    This site was built by 
    <a href="http://www.copterlabs.com/" target="_blank">Copter Labs</a>. Here 
    are some links and contact info that you may find helpful.
</p>
<h4>Support</h4>
<ul>
    <li>Email: <a href="mailto:info@copterlabs.com">info@copterlabs.com</a></li>
    <li>Phone: <a href="callto:+18552678375">+1 855-267-8375</a></li>
</ul>
<h4>Connect with Copter Labs</h4>
<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Ffacebook.com%2Fcopterlabs&amp;send=false&amp;layout=standard&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=35&amp;appId=121970801204701" 
        scrolling="no" frameborder="0" 
        style="border:none; overflow:hidden; width:450px; height:35px;" 
        allowTransparency="true"></iframe>
<p>
    You can find Copter Labs on 
    <a href="http://twitter.com/copterlabs" target="_blank">Twitter</a> and 
    <a href="http://facebook.com/copterlabs" target="_blank">Facebook</a>.
</p>
<h4>Referral Bonus</h4>
<p>
    Don't forget that <strong>Copter Labs pays $100 for each client you refer 
    to us.</strong> It's our way of saying thanks for spreading the word about 
    Copter Labs! If you have questions about how it works, just 
    <a href="mailto:ali.porter@copterlabs.com">ask Ali</a>.
</p>
<?php
}

/**
 * Adds a notice at the top of the WordPress admin screens with a message
 * @return void
 * @since  2.0.0
 */
function rw_progress_alert(  ) {
    if (!current_user_can('manage_rotorwash')) {
        return FALSE;
    }

    $opts = get_option('rw_theme_settings');

    if (!get_field('google_analytics_id', 'option')):
        $nag_message = '<strong>Important:</strong> In order to track visitors to your site, <a href="' 
                     . get_bloginfo('wpurl') 
                     . '/wp-admin/admin.php?page=acf-options-analytics-settings">please add your Google Analytics ID here.</a>';
?>
<script type="text/javascript">
jQuery(function($){
    $('.wrap > h2')
        .parent().prev()
        .after('<div class="update-nag"><?php echo $nag_message; ?></div>');
});
</script>
<?php
    endif;
}
add_action('admin_head','rw_progress_alert');

/**
 * Custom class for creating the "Developer" role
 * @since  2.0.0
 */
class RW_Role
{

    /**
     * All WordPress caps go here, plus a new one for managing RotorWash
     * @var array
     */
    private static $_default_caps = array(
        'activate_plugins' => TRUE,
        'delete_others_pages' => TRUE,
        'delete_others_posts' => TRUE,
        'delete_pages' => TRUE,
        'delete_plugins' => TRUE,
        'delete_posts' => TRUE,
        'delete_private_pages' => TRUE,
        'delete_private_posts' => TRUE,
        'delete_published_pages' => TRUE,
        'delete_published_posts' => TRUE,
        'edit_dashboard' => TRUE,
        'edit_files' => TRUE,
        'edit_others_pages' => TRUE,
        'edit_others_posts' => TRUE,
        'edit_pages' => TRUE,
        'edit_posts' => TRUE,
        'edit_private_pages' => TRUE,
        'edit_private_posts' => TRUE,
        'edit_published_pages' => TRUE,
        'edit_published_posts' => TRUE,
        'edit_theme_options' => TRUE,
        'export' => TRUE,
        'import' => TRUE,
        'list_users' => TRUE,
        'manage_categories' => TRUE,
        'manage_links' => TRUE,
        'manage_options' => TRUE,
        'moderate_comments' => TRUE,
        'promote_users' => TRUE,
        'publish_pages' => TRUE,
        'publish_posts' => TRUE,
        'read_private_pages' => TRUE,
        'read_private_posts' => TRUE,
        'read' => TRUE,
        'remove_users' => TRUE,
        'switch_themes' => TRUE,
        'upload_files' => TRUE,
        'create_product' => TRUE,
        'update_core' => TRUE,
        'update_plugins' => TRUE,
        'update_themes' => TRUE,
        'install_plugins' => TRUE,
        'install_themes' => TRUE,
        'delete_themes' => TRUE,
        'edit_plugins' => TRUE,
        'edit_themes' => TRUE,
        'edit_users' => TRUE,
        'create_users' => TRUE,
        'delete_users' => TRUE,
        'unfiltered_html' => TRUE,

        // Custom capability to restrict certain actions to this role only
        'manage_rotorwash' => TRUE,

        // Allows this role to manage the Gravity Forms MailChimp extension
        'gravityforms_mailchimp' => TRUE,
        'gravityforms_mailchimp_uninstall' => TRUE,
    );

    /**
     * Initializes the role if required
     * @return  void
     */
    public function __construct(  ) {
        // If the role hasn't already been created, add it and set capabilities
        if (!get_role('developer')) {
            self::add_developer_role();
        }
    }

    /**
     * Sets up the new role and its capabilities
     * @return  void
     */
    public static function add_developer_role(  ) {
        add_role('developer', 'Developer', self::$_default_caps);
    }

}
