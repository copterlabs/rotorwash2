RotorWash2
==========

An updated default WordPress theme used in all Copter Labs projects, featuring 
Bootstrap, easy admin tools for site creation, and more.

This is intended to be used as a parent theme. A starter child theme exists; 
[get it here][rw2-child-starter].


Documentation
-------------

For documentation, check out [the wiki][wiki].


Issues
------

To view or report issues, please use [the issue tracker][issues].

Before you submit a new issue, please check through the issue tracker to make sure it doesn't already exist.

If the problem is something you can fix, fix it and submit a pull request.


Changelog
---------

### 3.2.2

* Removed unused ACF images
* Changed the testimonial image custom field to return an object instad of an ID for consistency
* Added a new custom field for the button text on services pages
* Fixed a broken link in the Google Analytics nag
* Changed testimonial images to use "object" return type for consistency with services and products
* Organized admin menu and added dashicons to custom post types
* Updated the Twitter init to include the Twitter follow button
* Downgraded jQuery to 1.10.2 for older IE support
* Added a function to build Twitter intents for tweets URLs
* Fixed a bug in the bit.ly link shortening function
* Included Facebook "follow" buttons in the FB init call
* Force ACF to save on all page saves to avoid an edge case where custom fields weren't saving
* Added .post img { .img-responsive; } to wp-classes.less
* Added an is_object() check to bit.ly response handling to avoid an error
* Added default support for dashboard icons to avoid a notice
* Added WP_Bootstrap_Navwalker so dropdown menus and active / current page classes will utilize the Bootstrap framework. https://github.com/twittem/wp-bootstrap-navwalker

### 3.2.1

* Changed .align(s) in wp-classes.less to allow for proper positioning of .wp-caption(s)
* Fixed a bug in post type archive displays
* Fixed a bug with Facebook like boxes not displaying under certain circumstances
* Removed unused theme-customizer.js
* Added a JS tweak for loading Twitter scripts
* Fixed a bug in IE where conditional comments were being ignored
* Fixed "Create your menu" link in navbar
* Added function reference includes/readme.md
* Added shortcodes for the alert and lead Bootstrap components
* Updated the styles for post meta to look a little better by default
* Added .placeholder(@placeholder-color: #666; @placeholder-style: italic) mixin to rw2-mixins.less
* Added pagination alignment parameter (defaulted to center) to rw_pagination() in extra.php
* Added responsive headings mixin .responsive-headings(@breakpoint; @size-percentage)
* Added !important to .aligncenter in wp-classes.less to override .img-retina's display: inline-block
* Updated the pagination function to use the range, and added first/last page buttons
* Updated ACF and the repeater/content block add-ons to the latest version

### 3.2.0

* Abstracted the column wrappers to separate files for rapid template mods

### 3.1.2

* Fixed ACF issue where child themes couldn't add custom fields using PHP

### 3.1.1

* Fixed a bug in the Bootstrap pagination function
* Added a setting to allow pagination markup to be returned or echoed

### 3.1.0

* Set comments to use Bootstrap styles by default
* Added helpful link info to the 404 email link

### 3.0.0

* Fixed an issue with WP aligment classes in tables
* Added Bootstrap pagination support
* Updated Bootstrap to v3.03
* Removed ShareThis
* Removed hard-coded G+ badge initialization
* Added a check to allow regular plugin usage of ACF
* Removed the theme customizer that wasn't being used
* Added minimum clearance to see the custom Copter dashboard module

### 2.3.1

* Major refactoring of code
* Added details to DocBlocks
* Wrote the first iteration of documentation in the wiki

### 2.3.0

* Added a killswitch for RW2-specific custom post type fields in case a child theme needs to overwrite the defaults
* Moved releases to the master branch instead of develop

### 2.2.2

* Added support for native WordPress comments (using Bootstrap markup/styles)
* Removed unnecessary submodules
* Corrected alignment bugs in the WordPress alignment and caption classes
* Minor bugfixes

### 2.2.1

* Updated the Bitly API call to use OAuth2 (instead of deprecated login/key)

### 2.2.0

* Updated the contact info on the Copter Dashboard widget
* Added link shortening via Bitly
* Added a utility function for making cURL requests
* Made minor syntax and ARIA adjustments

### 2.1.1

* Added the "text" attribute to the Twitter button config
* Added comments to new functions

### 2.1.0

* Added social button functions
* Included Pinterest's JavaScript in the social initialization function
* Miscellaneous refactoring

### 2.0.0

* Changed custom post types to use singular slugs
* Removed custom admin pages in favor of ACF options
* Removed unused stylesheets
* Changed templates to allow jumbotrons outside of containers
* Stylesheet tweaks
* Miscellaneous bugfixes

### 1.0.0

* Initial release


Versioning
----------

For transparency and insight into our release cycle, and for striving to maintain backward compatibility, this theme will be maintained under the Semantic Versioning guidelines as much as possible.

Releases will be numbered with the following format:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backward compatibility bumps the major (and resets the minor and patch)
* New additions without breaking backward compatibility bumps the minor (and resets the patch)
* Bug fixes and misc changes bumps the patch

For more information on SemVer, please visit [http://semver.org/](http://semver.org/).


Authors
-------

**Jason Lengstorf**

+ [http://twitter.com/jlengstorf](http://twitter.com/jlengstorf)
+ [http://github.com/jlengstorf](http://github.com/jlengstorf)

**Alex Newman**

+ [http://twitter.com/thedotmack](http://twitter.com/thedotmack)
+ [http://github.com/thedotmack](http://github.com/thedotmack)

**Roger Stringer**

+ [http://twitter.com/freekrai](http://twitter.com/freekrai)
+ [http://github.com/freekrai](http://github.com/freekrai)


Copyright and License
---------------------

Copyright 2013 Copter Labs, Inc.

[rw2-child-starter]: https://bitbucket.org/copterlabs/rotorwash2-child-theme-starter
[wiki]: https://bitbucket.org/copterlabs/rotorwash2/wiki
[issues]: https://bitbucket.org/copterlabs/rotorwash2/issues
