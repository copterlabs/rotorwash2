<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.  The actual display of comments is
 * handled by a callback to rw_comment which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage RotorWash
 * @since RotorWash 1.0
 */

if (post_password_required()) {
    return;
}

if (have_comments()):

?>
        <h3 id="comments-title">Comments for This Entry</h3>

        <ul id="post-comments" class="media-list">
            <?php wp_list_comments('callback=rw2_comment_walker'); ?> 
        </ul>
<?php 

endif;

rw2_comments_form();
